
package com.aggregator.flight;

import android.content.Context;

import com.aggregator.flight.network.FlightNetworkClient;
import com.aggregator.flight.network.NetworkClient;

public class AppProvider {
    private static final String LOG_TAG = "AppProvider";

    private static final String FLIGHT_LIST_ENDPOINT = "http://www.mocky.io/v2/5979c6731100001e039edcb3";
    private static AppProvider INSTANCE = null;
    private final Context mApplicationContext;

    private AppProvider(Context context) {
        mApplicationContext = context;
        NetworkClient.init(mApplicationContext);
    }

    public static void init(Context context) {
        INSTANCE = new AppProvider(context);
    }

    public static AppProvider get() {
        if (null == INSTANCE) {
            throw new RuntimeException("AppProvider not initialized");
        }
        return INSTANCE;
    }

    public FlightNetworkClient getFlightNetworkClient() {
        return new FlightNetworkClient(
                FLIGHT_LIST_ENDPOINT,
                NetworkClient.getRequestQueue()
        );
    }
}
