
package com.aggregator.flight.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class NetworkClient {
    private static RequestQueue mRequestQueue;

    private NetworkClient() {
        // no instances
    }

    /**
     * To initialize single request queue for Application
     * @param context Application context
     */
    public static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static RequestQueue getRequestQueue() {
        return mRequestQueue;
    }
}
