
package com.aggregator.flight.network;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class FlightNetworkClient {

    private final String mUrl;
    private final RequestQueue mRequestQueue;

    public FlightNetworkClient(String url, RequestQueue requestQueue) {
        this.mUrl = url;
        this.mRequestQueue = requestQueue;
    }

    public void getFlightList(String fromCity, String toCity, ResponseCallback callback) {
        String urlPath = "";
        try {
            sendRequest(Request.Method.GET,
                    urlPath,
                    null,
                    callback);
        } catch (Exception ex) {
            callback.onFailure(ex);
        }
    }

    private void sendRequest(int method, final String urlPath, JSONObject params, final ResponseCallback callback) {
        String completeUrl = String.format("%s%s", mUrl, urlPath);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                method,
                completeUrl,
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFailure(error.getCause());
                    }
                }
        );
        mRequestQueue.add(jsonObjectRequest);
    }

    public interface ResponseCallback {
        void onSuccess(JSONObject jsonObject);

        void onFailure(Throwable throwable);
    }
}