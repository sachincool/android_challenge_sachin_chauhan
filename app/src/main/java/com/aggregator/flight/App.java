
package com.aggregator.flight;

import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppProvider.init(getApplicationContext());
    }
}