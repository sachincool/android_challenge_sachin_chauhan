package com.aggregator.flight.entities;


import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Flight pojo class, also handles json processing
 */
public class Flight implements Comparator<ProviderFare> {
    public ProviderFare getMinimumFareProvider() {
        return minimumFareProvider;
    }

    public String getAirlineName() {
        return airlineName;
    }

    private ProviderFare minimumFareProvider;
    private String airlineName;

    public List<ProviderFare> getProviderFares() {
        return providerFares;
    }

    private List<ProviderFare> providerFares = new ArrayList<>();
    private String originCode;
    private String destinationCode;
    private long departureTime;
    private long arrivalTime;
    private String flightClass;


    public Flight(@NonNull JSONObject jsonObject, @NonNull JSONObject appendixJSONObject) {
        originCode = jsonObject.optString("originCode");
        destinationCode = jsonObject.optString("destinationCode");
        arrivalTime = jsonObject.optLong("arrivalTime");
        departureTime = jsonObject.optLong("departureTime");
        airlineCode = jsonObject.optString("airlineCode");
        airlineName = appendixJSONObject.optJSONObject("airlines").optString(airlineCode);
        flightClass = jsonObject.optString("class");

        JSONArray providerFaresArray = jsonObject.optJSONArray("fares");
        for (int i=0; i<providerFaresArray.length(); i++) {
            ProviderFare providerFare = new ProviderFare(
                    providerFaresArray.optJSONObject(i),
                    appendixJSONObject.optJSONObject("providers")
            );
            providerFares.add(
                providerFare
            );

        }
        Collections.sort(providerFares, this);
        minimumFareProvider = providerFares.get(0);
    }

    private Flight(Flight flight) {
        originCode = flight.getOriginCode();
        destinationCode = flight.getDestinationCode();
        arrivalTime = flight.getArrivalTime();
        departureTime = flight.getDepartureTime();
        airlineCode = flight.getAirlineCode();
        airlineName = flight.getAirlineName();
        flightClass = flight.getFlightClass();
        providerFares = flight.getProviderFares();
        minimumFareProvider = flight.getMinimumFareProvider();
    }

    public String getOriginCode() {
        return originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    private String airlineCode;

    @Override
    public int compare(ProviderFare o1, ProviderFare o2) {
        return o1.getFare() - o2.getFare();
    }

    public List<Flight> getProviderFaresFlights() {
        List<Flight> flights = new ArrayList<>();
        List<ProviderFare> providerFares = getProviderFares();
        for (ProviderFare providerFare : providerFares) {
            Flight flight = new Flight(this);
            flight.minimumFareProvider = providerFare;
            flights.add(flight);
        }
        return flights;
    }
}
