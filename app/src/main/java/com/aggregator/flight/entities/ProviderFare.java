package com.aggregator.flight.entities;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * ProviderFare class
 */
public class ProviderFare {

    private final String fightProvider;
    private final String providerId;
    private final int fare;

    ProviderFare(@NonNull JSONObject jsonObject, @NonNull JSONObject providersJSONObject) {
        providerId = jsonObject.optString("providerId");
        fightProvider = providersJSONObject.optString(providerId);
        fare = jsonObject.optInt("fare");
    }

    public int getFare() {
        return fare;
    }

    public String getFightProvider() {
        return fightProvider;
    }
}
