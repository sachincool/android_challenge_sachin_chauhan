package com.aggregator.flight.ui.flightList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aggregator.flight.R;
import com.aggregator.flight.entities.Flight;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * {@link RecyclerView.Adapter} that can display Flights and makes a call to the
 * specified {@link OnFlightListInteractionListener}.
 */
public class FlightListRecyclerViewAdapter extends RecyclerView.Adapter<FlightListRecyclerViewAdapter.ViewHolder> {

    private final List<Flight> mValues;
    final OnFlightListInteractionListener mListener;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

    final String RUPEE_SYMBOL = "₹";

    FlightListRecyclerViewAdapter(List<Flight> items, OnFlightListInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public static FlightListRecyclerViewAdapter newInstance(List<Flight> flightList, OnFlightListInteractionListener flightListPresenter) {
        return new FlightListRecyclerViewAdapter(flightList, flightListPresenter);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_flight, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mFlight = mValues.get(position);
        holder.mFlightName.setText(holder.mFlight.getAirlineName());
        holder.mStartTime.setText(simpleDateFormat.format(new Date(holder.mFlight.getDepartureTime())));
        holder.mEndTime.setText(simpleDateFormat.format(new Date(holder.mFlight.getArrivalTime())));
        holder.mFare.setText(String.format("%s %s", RUPEE_SYMBOL, holder.mFlight.getMinimumFareProvider().getFare()));
        holder.mFlightClass.setText(holder.mFlight.getFlightClass());
        holder.mProviderName.setText(holder.mFlight.getMinimumFareProvider().getFightProvider());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) mListener.onFlightClick(holder.mFlight);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private final TextView mFlightName;
        private final TextView mStartTime;
        private final TextView mEndTime;
        private final TextView mFare;
        private final TextView mFlightClass;
        private final TextView mProviderName;

        public Flight mFlight;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mFlightName = (TextView) view.findViewById(R.id.tv_flight_name);
            mProviderName = (TextView) view.findViewById(R.id.tv_provider_name);
            mFlightClass = (TextView) view.findViewById(R.id.tv_flight_class);
            mStartTime = (TextView) view.findViewById(R.id.tv_flight_start_time);
            mEndTime = (TextView) view.findViewById(R.id.tv_flight_end_time);
            mFare = (TextView) view.findViewById(R.id.tv_fight_fare);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mFlightName.getText() + "'";
        }
    }

    public interface OnFlightListInteractionListener {
        void onFlightClick(Flight Flight);
    }
}