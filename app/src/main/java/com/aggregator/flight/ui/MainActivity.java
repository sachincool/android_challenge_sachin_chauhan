package com.aggregator.flight.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aggregator.flight.R;
import com.aggregator.flight.entities.Flight;
import com.aggregator.flight.ui.flightList.FlightListFragment;
import com.aggregator.flight.ui.flightList.FlightListRecyclerViewAdapter;
import com.aggregator.flight.ui.flightList.FlightProviderDetailsFragment;

import static com.aggregator.flight.ui.flightList.FlightListFragment.DESTINATION;
import static com.aggregator.flight.ui.flightList.FlightListFragment.SHOW_BACK_BUTTON;
import static com.aggregator.flight.ui.flightList.FlightListFragment.SOURCE;

public class MainActivity extends BaseActivity implements FlightListRecyclerViewAdapter.OnFlightListInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_BACK_BUTTON, false);
        bundle.putString(SOURCE, "Delhi");
        bundle.putString(DESTINATION, "Mumbai");
        FlightListFragment fragment = FlightListFragment.newFlightListFragment(bundle);
        addFragment(R.id.layout_container,
                fragment,
                this, null);
    }

    @Override
    public void onFlightClick(Flight flight) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_BACK_BUTTON, true);
        FlightProviderDetailsFragment fragment = FlightProviderDetailsFragment.newFlightProviderDetailsFragment(bundle);
        fragment.setFlight(flight);
        addFragment(R.id.layout_container,
                fragment,
                this, null);
    }

    @Override
    public void goBack() {
        Fragment topFragment = getSupportFragmentManager().findFragmentById(R.id.layout_container);

        if (this.getSupportFragmentManager().getBackStackEntryCount() > 1) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.remove(topFragment);
            transaction.commit();
            getSupportFragmentManager().popBackStack();
        } else {
            super.goBack();
        }
    }
}
