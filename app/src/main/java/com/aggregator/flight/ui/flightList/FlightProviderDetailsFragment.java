
package com.aggregator.flight.ui.flightList;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aggregator.flight.R;
import com.aggregator.flight.entities.Flight;
import com.aggregator.flight.ui.BaseFragment;
import com.aggregator.flight.ui.uicomponent.AppBar;

import java.util.ArrayList;
import java.util.List;

public class FlightProviderDetailsFragment extends BaseFragment {

    public static final String SHOW_BACK_BUTTON = "show_back_button";
    private static final String PAGE_HEADING = "Flight Provider Details";

    private FlightListRecyclerViewAdapter.OnFlightListInteractionListener mListener;
    private RecyclerView mRecyclerView;

    private FlightListRecyclerViewAdapter mFlightListRecyclerViewAdapter;
    private boolean mShowBackButton = true;
    private Flight mFlight;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FlightProviderDetailsFragment() {
    }

    public static FlightProviderDetailsFragment newFlightProviderDetailsFragment(Bundle bundle) {
        FlightProviderDetailsFragment fragment = new FlightProviderDetailsFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowBackButton = getArguments().getBoolean(SHOW_BACK_BUTTON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_flight_provider_details, container, false);

        AppBar appBar = AppBar.newInstance(getContext(), PAGE_HEADING, mShowBackButton);
        setUpAppBar(view, appBar);

        mRecyclerView = view.findViewById(R.id.rv_flight);

        List<Flight> flights = mFlight.getProviderFaresFlights();
        mFlightListRecyclerViewAdapter = FlightListRecyclerViewAdapter.newInstance(flights, null);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(mFlightListRecyclerViewAdapter);

        return view;
    }

    public void setFlight(Flight flight) {
        mFlight = flight;
    }
}