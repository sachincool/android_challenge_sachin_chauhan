
package com.aggregator.flight.ui.flightList;

import com.aggregator.flight.ui.BaseView;

interface FlightListView extends BaseView {
    void notifyDataSetChanged();
}