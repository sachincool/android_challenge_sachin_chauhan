package com.aggregator.flight.ui.flightList;

import android.util.Log;

import com.aggregator.flight.AppProvider;
import com.aggregator.flight.entities.Flight;
import com.aggregator.flight.network.FlightNetworkClient;
import com.aggregator.flight.ui.BasePresenter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


class FlightListPresenter extends BasePresenter<FlightListView> {
    private static final String LOG_TAG = "FlightListPresenter";
    private String mDesCity;
    private String mSrcCity;

    private FlightListPresenter() {}

    public static FlightListPresenter newInstance() {
        return new FlightListPresenter();
    }

    private Boolean httpRequestPending = false;

    private List<Flight> flightList = new ArrayList<>();

    @Override
    public void attachView(FlightListView mvpView) {
        super.attachView(mvpView);
        updateFlightList();
    }

    void updateFlightList() {
        if(httpRequestPending){
            return;
        }
        showProgress(true);
        httpRequestPending = true;

        AppProvider.get().getFlightNetworkClient().getFlightList(getSrcCity(), getDesCity(), new FlightNetworkClient.ResponseCallback() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                if (null == jsonObject) {
                    return;
                }
                Log.d(LOG_TAG, jsonObject.toString());
                JSONObject appendix = jsonObject.optJSONObject("appendix");
                JSONArray jsonFlights = jsonObject.optJSONArray("flights");

                flightList.clear();
                for (int i=0; i<jsonFlights.length(); i++) {
                    flightList.add(new Flight(jsonFlights.optJSONObject(i), appendix));
                }
                showProgress(false);
                getMvpView().notifyDataSetChanged();
                httpRequestPending = false;
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e(LOG_TAG, "Get Flight list error");
                showProgress(false);
                getMvpView().notifyDataSetChanged();
                httpRequestPending = false;
            }
        });

    }

    private String getDesCity() {
        return mDesCity;
    }

    private String getSrcCity() {
        return mSrcCity;
    }

    void setFlightDetails(String src, String des) {
        mSrcCity = src;
        mDesCity = des;
    }

    private void showProgress(boolean show) {
        if (null != getMvpView()) getMvpView().showProgress(show);
    }

    void setFlightList(List<Flight> flightList) {
        this.flightList = flightList;
    }

    public void filterByMinimumFare() {
        Collections.sort(this.flightList, new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                return o1.getMinimumFareProvider().getFare() - o2.getMinimumFareProvider().getFare();
            }
        });
        getMvpView().notifyDataSetChanged();
    }

    public void filterByLandingTime() {
        Collections.sort(this.flightList, new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                return (int)(o1.getArrivalTime() - o2.getArrivalTime());
            }
        });
        getMvpView().notifyDataSetChanged();
    }

    public void filterByTakeOffTime() {
        Collections.sort(this.flightList, new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                return (int)(o1.getDepartureTime() - o2.getDepartureTime());
            }
        });
        getMvpView().notifyDataSetChanged();
    }
}