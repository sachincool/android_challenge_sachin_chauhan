package com.aggregator.flight.ui;

public interface BaseView {

    void showToastMessage(String text);

    void goBack();

    void close();

    void showProgress(boolean show);

    void showProgress(boolean show, String progressString);
}

