package com.aggregator.flight.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aggregator.flight.R;
import com.aggregator.flight.ui.uicomponent.AppBar;


/*
 * Abstract Fragment that every other Fragment in this application must implement.
 */
public abstract class BaseFragment extends Fragment implements BaseView {

    private BaseActivity baseActivity = null;
    private AppBar mAppBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            baseActivity = (BaseActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup =  (ViewGroup) inflater.inflate(R.layout.fragment_base, container, false);
        onCreateViewDelegate(inflater, viewGroup, savedInstanceState);
        return viewGroup;
    }

    protected void onCreateViewDelegate(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    }

    protected void setUpAppBar(@NonNull ViewGroup view) {
        AppBar appBar = new AppBar(baseActivity);
        setUpAppBar(view, appBar);
    }

    protected void setUpAppBar(@NonNull ViewGroup view, @NonNull AppBar appBar) {
        ViewGroup viewGroup = view.findViewById(R.id.layout_holder);
        viewGroup.removeView(mAppBar);
        viewGroup.addView(appBar, 0 ,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mAppBar = appBar;
        mAppBar.setBackButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
        showAppBar(true);
    }

    protected void showAppBar(boolean show) {
        if (null != mAppBar) {
            mAppBar.setVisibility(show? View.VISIBLE: View.GONE);
        }
    }

    @Override
    public void showToastMessage(String text) {
        if (null != baseActivity) baseActivity.showToastMessage(text);
    }

    @Override
    public void goBack() {
        if (null != baseActivity) baseActivity.goBack();
    }

    @Override
    public void close() {
        if (null != baseActivity) baseActivity.close();
    }

    @Override
    public void showProgress(boolean show) {
        if (null != baseActivity) baseActivity.showProgress(show);
    }

    @Override
    public void showProgress(boolean show, String progressString) {
        if (null != baseActivity) baseActivity.showProgress(show, progressString);
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }
}