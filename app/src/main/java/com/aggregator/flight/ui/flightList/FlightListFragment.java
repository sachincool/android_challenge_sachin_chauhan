
package com.aggregator.flight.ui.flightList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.aggregator.flight.R;
import com.aggregator.flight.entities.Flight;
import com.aggregator.flight.ui.BaseFragment;
import com.aggregator.flight.ui.uicomponent.AppBar;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link FlightListRecyclerViewAdapter.OnFlightListInteractionListener}
 * interface.
 */
public class FlightListFragment extends BaseFragment implements FlightListView {

    public static final String SHOW_BACK_BUTTON = "show_back_button";
    public static final String DESTINATION = "destination";
    public static final String SOURCE = "source";

    private FlightListRecyclerViewAdapter.OnFlightListInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mPullToRefresh;

    private FlightListPresenter mFlightListPresenter = FlightListPresenter.newInstance();
    private FlightListRecyclerViewAdapter mFlightListRecyclerViewAdapter;
    private List<Flight> mFlightList;
    private boolean mShowBackButton = true;
    private String mSource;
    private String mDestination;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FlightListFragment() {
    }

    public static FlightListFragment newFlightListFragment(Bundle bundle) {
        FlightListFragment fragment = new FlightListFragment();
        Bundle args = new Bundle();
        args.putAll(bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowBackButton = getArguments().getBoolean(SHOW_BACK_BUTTON);
            mSource = getArguments().getString(SOURCE);
            mDestination = getArguments().getString(DESTINATION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_flight_list, container, false);

        AppBar appBar = AppBar.newInstance(getContext(), String.format("%s to %s", mSource, mDestination), mShowBackButton);
        setUpAppBar(view, appBar);

        mRecyclerView = view.findViewById(R.id.rv_flight);
        mPullToRefresh = view.findViewById(R.id.pullToRefresh);

        view.findViewById(R.id.tv_filter_fare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlightListPresenter.filterByMinimumFare();
            }
        });

        view.findViewById(R.id.tv_filter_landing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlightListPresenter.filterByLandingTime();
            }
        });

        view.findViewById(R.id.tv_filter_take_off).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlightListPresenter.filterByTakeOffTime();
            }
        });

        mFlightList = new ArrayList<Flight>();
        mFlightListPresenter.setFlightList(mFlightList);

        mFlightListRecyclerViewAdapter = FlightListRecyclerViewAdapter.newInstance(mFlightList, mListener);

        mFlightListPresenter.setFlightDetails(mSource, mDestination);
        mFlightListPresenter.attachView(this);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(mFlightListRecyclerViewAdapter);

        mPullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFlightListPresenter.updateFlightList();
                mPullToRefresh.setRefreshing(false);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FlightListRecyclerViewAdapter.OnFlightListInteractionListener) {
            mListener = (FlightListRecyclerViewAdapter.OnFlightListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void notifyDataSetChanged() {
        mFlightListRecyclerViewAdapter.notifyDataSetChanged();
    }
}